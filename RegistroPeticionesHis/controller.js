const { Console } = require("console");
const oracledb = require("oracledb");
const { parse } = require("path");

async function data() {
  try {
    let connection;
    connection = await oracledb.getConnection({
      user: "xgpcprd",
      password: "HIS510",
      connectString: "172.24.19.79:1521/HIS510",
    });

    const result = await connection.execute(
      "SELECT id_registro," +
        " " +
        "		cli.codigo_cliente AS pn_cod_cliente," +
        " " +
        "       TO_NUMBER(con.codigo_conver) AS pn_id_sede," +
        " " +
        "       1 AS pn_id_red," +
        " " +
        "       DECODE(col.CODIGO_CONVER, NULL, TRIM(per.codigo_colegiado), TRIM(col.CODIGO_CONVER) ) AS pv_cod_cmp," +
        " " +
        "		pet.codmed AS pv_id_medic_ori," +
        " " +
        "       per.tipo_dni_pk AS pv_tdoc_ide_med," +
        " " +
        "       TRIM(per.fpers_doc) AS pv_ndoc_ide_med," +
        " " +
        "       UPPER(TRIM(pet.ape1med))  AS pv_ape_pat_med," +
        " " +
        "       UPPER(TRIM(pet.ape2med))  AS pv_ape_mat_med," +
        " " +
        "       UPPER(TRIM(pet.nommed))  AS pv_nombres_med," +
        " " +
        "       TRIM(per.email) AS pv_email_med," +
        " " +
        "       TRIM(per.telefono) AS pv_telefono_med," +
        " " +
        "       cli.tipo_dni_pk AS pv_tdoc_ide_pac," +
        " " +
        "       TRIM(cli.codigo1) AS pv_ndoc_ide_pac," +
        " " +
        "       pet.dni AS dni_gpc," +
        " " +
        "       pet.ape1pac AS pv_ape_pat_pac," +
        " " +
        "       pet.ape2pac AS pv_ape_mat_pac," +
        " " +
        "       pet.nompac AS pv_nombres_pac," +
        " " +
        "       DECODE (pet.sexo, '1', 'M', 'F') AS pv_genero," +
        " " +
        "       TRIM(pet.telefono) AS pv_telefono_pac," +
        " " +
        "       TRIM(regexp_replace(cli.email, '[^A-Za-z0-9ÁÉÍÓÚáéíóú@/.,_-]', '')) AS pv_email_pac," +
        " " +
        "       TO_CHAR(pet.fecnac,'dd/mm/yyyy') AS pv_fecha_nac," +
        " " +
        "       pet.numpeti AS pn_id_peticion," +
        " " +
        "       'HIS' AS pv_cod_sis_ext," +
        " " +
        "       1 AS pn_id_sistema," +
        " " +
        "       pet.nhc AS pv_num_hist_clin," +
        " " +
        "       FLOOR(MONTHS_BETWEEN(pet.fecha_reg, pet.fecnac)/12) AS pn_edad_pac," +
        " " +
        "       'L' AS pv_tipo," +
        " " +
        "       pet.ambito AS pv_ambito," +
        " " +
        "       TO_CHAR(pet.fecha_reg,'dd/mm/yyyy') AS pv_fecha_pet FROM xgpcprd.lab_peticiones pet" +
        " " +
        "LEFT JOIN xhisprd.hc ON TO_NUMBER(pet.nhc) = TO_NUMBER(hc.nhc) AND hc.activa_sn = 1" +
        " " +
        "LEFT JOIN xhisprd.clientes cli ON hc.codigo_cliente = cli.codigo_cliente" +
        " " +
        "LEFT JOIN xhisprd.fpersona per ON pet.codmed = per.codigo_personal" +
        " " +
        "LEFT JOIN servicios ser ON ser.codserv = pet.codserv" +
        " " +
        "LEFT JOIN  xhisprd.conversion con ON  SUBSTR(ser.nomserv, 0,3) = con.codigo_novahis  AND con.tipo_conver_pk = 100360" +
        " " +
        "LEFT JOIN  xhisprd.conversion col ON  col.tipo_conver_pk = 690526 AND TRIM(per.FPERS_DOC) = TRIM(col.codigo_novahis)" +
        " " +
        "WHERE pet.pagado = '2'" +
        " " +
        "  ORDER BY pet.id_registro ASC "
    );

    hayConexionGPC = "SI";
    console.log(result.rows.length);
    var ID_REGISTRO = result.metaData.findIndex(
      (x) => x.name === "ID_REGISTRO"
    );
    var PN_COD_CLIENTE = result.metaData.findIndex(
      (x) => x.name === "PN_COD_CLIENTE"
    );
    var PN_ID_SEDE = result.metaData.findIndex((x) => x.name === "PN_ID_SEDE");
    var PN_ID_RED = result.metaData.findIndex((x) => x.name === "PN_ID_RED");
    var PV_COD_CMP = result.metaData.findIndex((x) => x.name === "PV_COD_CMP");
    var PV_ID_MEDIC_ORI = result.metaData.findIndex(
      (x) => x.name === "PV_ID_MEDIC_ORI"
    );
    var PV_TDOC_IDE_MED = result.metaData.findIndex(
      (x) => x.name === "PV_TDOC_IDE_MED"
    );
    var PV_NDOC_IDE_MED = result.metaData.findIndex(
      (x) => x.name === "PV_NDOC_IDE_MED"
    );
    var PV_APE_PAT_MED = result.metaData.findIndex(
      (x) => x.name === "PV_APE_PAT_MED"
    );
    var PV_APE_MAT_MED = result.metaData.findIndex(
      (x) => x.name === "PV_APE_MAT_MED"
    );
    var PV_NOMBRES_MED = result.metaData.findIndex(
      (x) => x.name === "PV_NOMBRES_MED"
    );
    var PV_EMAIL_MED = result.metaData.findIndex(
      (x) => x.name === "PV_EMAIL_MED"
    );
    var PV_TELEFONO_MED = result.metaData.findIndex(
      (x) => x.name === "PV_TELEFONO_MED"
    );
    var PV_TDOC_IDE_PAC = result.metaData.findIndex(
      (x) => x.name === "PV_TDOC_IDE_PAC"
    );
    var PV_NDOC_IDE_PAC = result.metaData.findIndex(
      (x) => x.name === "PV_NDOC_IDE_PAC"
    );
    //var DNI_GPC = result.metaData.findIndex(x => x.name === 'DNI_GPC');
    var PV_APE_PAT_PAC = result.metaData.findIndex(
      (x) => x.name === "PV_APE_PAT_PAC"
    );
    var PV_APE_MAT_PAC = result.metaData.findIndex(
      (x) => x.name === "PV_APE_MAT_PAC"
    );
    var PV_NOMBRES_PAC = result.metaData.findIndex(
      (x) => x.name === "PV_NOMBRES_PAC"
    );
    var PV_GENERO = result.metaData.findIndex((x) => x.name === "PV_GENERO");
    var PV_TELEFONO_PAC = result.metaData.findIndex(
      (x) => x.name === "PV_TELEFONO_PAC"
    );
    var PV_EMAIL_PAC = result.metaData.findIndex(
      (x) => x.name === "PV_EMAIL_PAC"
    );
    var PV_FECHA_NAC = result.metaData.findIndex(
      (x) => x.name === "PV_FECHA_NAC"
    );
    var PN_ID_PETICION = result.metaData.findIndex(
      (x) => x.name === "PN_ID_PETICION"
    );
    var PV_COD_SIS_EXT = result.metaData.findIndex(
      (x) => x.name === "PV_COD_SIS_EXT"
    );
    var PN_ID_SISTEMA = result.metaData.findIndex(
      (x) => x.name === "PN_ID_SISTEMA"
    );
    var PV_NUM_HIST_CLIN = result.metaData.findIndex(
      (x) => x.name === "PV_NUM_HIST_CLIN"
    );
    var PN_EDAD_PAC = result.metaData.findIndex(
      (x) => x.name === "PN_EDAD_PAC"
    );
    var PV_TIPO = result.metaData.findIndex((x) => x.name === "PV_TIPO");
    var PV_AMBITO = result.metaData.findIndex((x) => x.name === "PV_AMBITO");
    var PV_FECHA_PET = result.metaData.findIndex(
      (x) => x.name === "PV_FECHA_PET"
    );
    //for (let i = 0; i < result.rows.length; i++) {
    for (let i = 0; i < 5; i++) {
      var listaPruebas = "";
      var numPruebas = "";
      var pv_numdoc_med = "";
      var pv_tipdoc_med = "";

      const sqlnumPruebas = await connection.execute("SELECT count(*) as numPruebas FROM lab_metodos where id_registro = " + result.rows[i][ID_REGISTRO]);
      numPruebas = sqlnumPruebas.rows[0][0];
      const codPrueba = await connection.execute("select metodo as codPrueba from lab_metodos where id_registro = " +result.rows[i][ID_REGISTRO]);

      for (let j = 0; j < numPruebas; j++) {
        if (j == 0) {
          listaPruebas = listaPruebas + codPrueba.rows[j];
        } else {
          listaPruebas = listaPruebas + "|" + codPrueba.rows[j];
        }
      }
      console.log("listaPruebas:  " + listaPruebas);

      if (
        result.rows[i][PV_TDOC_IDE_MED] == "" ||
        result.rows[i][PV_NDOC_IDE_MED] == ""
      ) {
        pv_numdoc_med = result.rows[i][PV_ID_MEDIC_ORI];
        pv_tipdoc_med = "0";
      } else {
        pv_numdoc_med = result.rows[i][PV_NDOC_IDE_MED];
        pv_tipdoc_med = result.rows[i][PV_TDOC_IDE_MED];
      }

      var codError = 0;
      console.log(
        result.rows[i][PN_ID_SEDE],
        result.rows[i][PN_ID_RED],
        result.rows[i][PV_COD_CMP],
        pv_tipdoc_med,
        pv_numdoc_med,
        result.rows[i][PV_ID_MEDIC_ORI],
        result.rows[i][PV_APE_PAT_MED],
        result.rows[i][PV_APE_MAT_MED],
        result.rows[i][PV_NOMBRES_MED],
        result.rows[i][PV_EMAIL_MED],
        result.rows[i][PV_TELEFONO_MED],
        result.rows[i][PN_COD_CLIENTE],
        result.rows[i][PV_TDOC_IDE_PAC],
        result.rows[i][PV_NDOC_IDE_PAC],
        result.rows[i][PV_APE_PAT_PAC],
        result.rows[i][PV_APE_MAT_PAC],
        result.rows[i][PV_NOMBRES_PAC],
        result.rows[i][PV_GENERO],
        result.rows[i][PV_TELEFONO_PAC],
        result.rows[i][PV_EMAIL_PAC],
        result.rows[i][PV_FECHA_NAC],
        result.rows[i][PN_ID_PETICION],
        result.rows[i][PV_COD_SIS_EXT],
        result.rows[i][PN_ID_SISTEMA],
        result.rows[i][PV_NUM_HIST_CLIN],
        result.rows[i][PN_EDAD_PAC],
        result.rows[i][PV_TIPO],
        result.rows[i][PV_AMBITO],
        result.rows[i][PV_FECHA_PET],
        listaPruebas
      );

      codError = registra_peticion (result.rows[i][PN_ID_SEDE],result.rows[i][PN_ID_RED],result.rows[i][PV_COD_CMP],pv_tipdoc_med,pv_numdoc_med,
        result.rows[i][PV_ID_MEDIC_ORI],result.rows[i][PV_APE_PAT_MED],result.rows[i][PV_APE_MAT_MED],result.rows[i][PV_NOMBRES_MED],result.rows[i][PV_EMAIL_MED],
        result.rows[i][PV_TELEFONO_MED],result.rows[i][PN_COD_CLIENTE],result.rows[i][PV_TDOC_IDE_PAC],result.rows[i][PV_NDOC_IDE_PAC],result.rows[i][PV_APE_PAT_PAC],
        result.rows[i][PV_APE_MAT_PAC],result.rows[i][PV_NOMBRES_PAC],result.rows[i][PV_GENERO],result.rows[i][PV_TELEFONO_PAC],result.rows[i][PV_EMAIL_PAC],
        result.rows[i][PV_FECHA_NAC],result.rows[i][PN_ID_PETICION],result.rows[i][PV_COD_SIS_EXT],result.rows[i][PN_ID_SISTEMA],result.rows[i][PV_NUM_HIST_CLIN],
        result.rows[i][PN_EDAD_PAC],result.rows[i][PV_TIPO],result.rows[i][PV_AMBITO],result.rows[i][PV_FECHA_PET],listaPruebas);
        
      if (codError > 0) {
        throw "ERROR";
      } else {
        const sqldeleteMet = await connection.execute(
          "delete lab_metodos where id_registro = " +
            result.rows[i][ID_REGISTRO]
        );
        console.log(sqldeleteMet);
        const sqldeletePet = await connection.execute(
          "delete lab_peticiones where id_registro = " +
            result.rows[i][ID_REGISTRO]
        );
        console.log(sqldeletePet);
      }
    }
  } catch (error) {
    if (hayConexionGPC == "NO") {
      console.log(
        "WebResultados-RegistroPeticionesHIS: ERROR al conectar con la BD del xGPC (" +
          error.message +
          ")"
      );
    } else {
      //logger.error(e.message);
      const sqlupdatePet = await connection.execute(
        "update lab_peticiones set pagado = 4 where  id_registro = " +
          result.rows[i][ID_REGISTRO]
      );
      console.log(sqlupdatePet);
    }
  }
}


async function insert_peticion(pn_id_sede, pn_id_red, pv_cod_cmp, pv_tdoc_ide_med, 
                               pv_ndoc_ide_med, pv_id_medic_ori, pv_ape_pat_med, pv_ape_mat_med, 
                               pv_nombres_med, pv_email_med, pv_telefono_med, pn_cod_cliente, pv_tdoc_ide_pac, 
                               pv_ndoc_ide_pac, pv_ape_pat_pac, pv_ape_mat_pac, pv_nombres_pac, 
                               pv_genero, pv_telefono_pac, pv_email_pac, pv_fecha_nac, 
                               pn_id_peticion, pv_cod_sis_ext, pn_id_sistema, pv_num_hist_clin, pn_edad_pac, 
                               pv_tipo, pv_ambito, pv_fecha_pet, pv_lista_pruebas)
  {
  let connection;
  var sql = "";
  try {

      connection = await oracledb.getConnection({
          user: "WEBRESINT",
          password: "WEBRESINT2017",
          connectString: "10.41.190.196:1521/SISOQAS"
      });

      sql += "DECLARE PN_LG_ID_LOGSEC NUMBER; NUM_PETICION NUMBER; ";
      sql += "BEGIN WEBRESINT.PQ_WEBRESULTADOS_DATOS.PR_INSERT_PETICION ( ";
      sql += pn_id_sede + ", " + pn_id_red + ", '" + pv_cod_cmp + "', '" + pv_tdoc_ide_med + "', '" + pv_ndoc_ide_med + "', '" + pv_id_medic_ori + "', '" + pv_ape_pat_med
      + "', '" + pv_ape_mat_med + "', '" + pv_nombres_med + "', '" + pv_email_med + "', '" + pv_telefono_med + "', " + pn_cod_cliente + ", '" + pv_tdoc_ide_pac + "', '" +
      pv_ndoc_ide_pac + "', '" + pv_ape_pat_pac + "', '" + pv_ape_mat_pac + "', '" + pv_nombres_pac + "', '" + pv_genero + "', '" + pv_telefono_pac + "', '" + pv_email_pac
      + "', '" + pv_fecha_nac + "', " + pn_id_peticion + ", '" + pv_cod_sis_ext + "', " + pn_id_sistema + ", '" + pv_num_hist_clin + "', " + pn_edad_pac + ", '" +
      pv_tipo + "', '" + pv_ambito + "', '" + pv_fecha_pet + "', '" + pv_lista_pruebas +"', PN_LG_ID_LOGSEC); ";
      sql += "END;";

      console.log(sql)

      const result = await connection.execute(sql);

      console.log(result)

      connection.close();

      return result

    //   var sentencia = "DECLARE "+
    //   "PN_LG_ID_LOGSEC NUMBER; "+
    //    "NUM_PETICION NUMBER; "+
    //  "BEGIN "+
    //  "SELECT MAX(ID_PETICION)+1 INTO NUM_PETICION FROM WEBRESINT.PETICION WHERE ID_PETICION LIKE '80%'; "+
    //  "WEBRESINT.PQ_WEBRESULTADOS_DATOS.PR_INSERT_PETICION (  "+
    //                                  "1 "+
    //                                ", 1 "+
    //                                ", '999998' "+
    //                                ", '1' "+
    //                                ", '44556677' "+
    //                                ", '' "+
    //                                ", 'PRUEBA' "+
    //                                ", 'AUNA' "+
    //                                ", 'MEDICO' "+
    //                                ", 'JESPINOZAM@AUNA.PE' "+
    //                                ", '9999999' "+
    //                                ", 290573 "+
    //                                ", 1 "+
    //                                ", '44449042' "+
    //                                ", 'ESPINOZA' "+
    //                                ", 'MERINO' "+
    //                                ", 'JOSE CARLOS' "+
    //                                ", 'M' "+
    //                                ", '234309' "+
    //                                ", 'JOSE.ESPINOZA.3D@GMAIL.COM' "+
    //                                ", '31/05/1987' "+
    //                                ", NUM_PETICION "+
    //                                ", 'HIS' "+
    //                                ", 1 "+
    //                                ", '251456' "+
    //                                ", '33' "+
    //                                ", 'L' "+
    //                                ", 'C' "+
    //                                ", TO_CHAR(SYSDATE,'DD/MM/YYYY') "+
    //                                ", '673|674' "+
    //                                ", PN_LG_ID_LOGSEC "+                
    //                                "); "+
    //     "DBMS_OUTPUT.put_line (PN_LG_ID_LOGSEC); "+
    //     "DBMS_OUTPUT.put_line ('NUM_PETICION: '||NUM_PETICION); "+
    //  "END;"

     //var sentencia = "select WEBRESINT.pq_webresultados_datos.fn_exist_prest_micro(30055844,20) as valor from dual"
      // console.log(sentencia)
      //   console.log(params)
      // const result = await connection.execute(sentencia);
      //   connection.execute(
      //     "call webresint.pq_webresultados_datos.PR_VALID_SEDE(1,1,'1');",
      //    // "BEGIN SEARCH_PROC(:p1, :p2, :ret); END;", // Yields the same result
      //     params,
      //     function (err, ret) {
      //       if (err) {
      //         console.log('failed', err);
      //       } else {
      //         console.log(ret);
      //       }
      //     }
      //   );

  } catch (error) {
      console.log(error);
  }
}

// insert_peticion(1, 1, "999998", "1", 
// "44556677", "", "PRUEBA", "AUNA", 
// "MEDICO", "JESPINOZAM@AUNA.PE", "9999999", 290573, 1, 
// "44449042", "ESPINOZA", "MERINO", "JOSE CARLOS", 
// "M", "234309", "JOSE.ESPINOZA.3D@GMAIL.COM", "31/05/1987", 
// 80000114, "HIS", 1 , "251456", "33", 
//   "L", "C", "10/03/2021" , "673|674");


