const { Console } = require("console");
const oracledb = require("oracledb");

async function data() {
  var texto = "";
  texto +=
    "H|^&|||Enterprise^CLTech|||||Enterprise^CLTech||P|2|20200929224647\n";
  texto +=
    "P|1|30055844|20||BERNAL^MAMANI^KATHERINE MABEL||19850312|F|||||029019^PORTUGAL PORTUGAL EDWARD MIGUEL||||||||10128603||20190603085836^000000|||||||||89 ^VSU GINECO-OBSTETRICIA CEX|C^HIS|||1|OR\n";
  texto +=
    "O|30055844|9002|ISIQUE|Enterprise|20200811151033|^|^|^|A|SANGRE CON EDTA|\n";
  texto +=
    "R|1|^^^20^305^^^^^TIEMPO DE COAGULACION Y SANGRIA|-||||128^TIEMPO DE COAGULACION Y SANGRI|F||^GPARIENTE||20190603085819|20190812100533|20190812092603|20190812092621|20190812092705|158^HEMATOLOGIA\n";
  texto += "L|1|N";

  var arrayLineas = ""; // arreglo que contiene por cada posición una linea del mensage original
  var numLineas = 0;

  //varaibles para obtención de datos de la trama
  var arrayCampos;
  var etiqueta = "";
  var subEtiqueta = "";
  var codTipo = "";
  var tipoFila = "";
  var numPeticion = "";
  var fuente = "";
  var codPrueba = "";
  var nomPrueba = "";
  var resPrueba = "";
  var resPruebaTitulo = "";
  nomPruebaTitulo = "";
  var codPerfil = "";
  var valor_cmi = "";
  var esResuladoFinal = "NO";
  var resFinal = "";

  //variables de error
  var textError = "";
  var msgError = "";

  var valorSubEtiquetaE = "EXÁMEN DIRECTO:";
  var valorSubEtiquetaG = "COLORACIÓN GRAM:";
  var valorSubEtiquetaC = "CONTEO DE COLONIAS:";
  var valorSubEtiquetaO = "GÉRMEN AISLADO:";
  var valorSubEtiquetaA = "ANTIBIOGRAMA:";
  var valorSubEtiquetaK = "PRUEBA DE AMINAS (KOH):";
  var valorSubEtiquetaY = "COLORACIÓN PARA CAMPYLOBACTER:";
  var valorSubEtiquetaR = "RESULTADO:";

  let connection;
  let connectionMirth;
  var hayConexionGPC = "NO";
  try {
    //CONEXION MIRTH
    connectionMirth = await oracledb.getConnection({
      user: "MIRTHPRD23",
      password: "M1r7H4unA23",
      connectString: "10.41.1.169:1521/PRD23",
    });

    // CONEXION WEBRESINT
    connection = await oracledb.getConnection({
      user: "WEBRESINT",
      password: "WEBRESINT2017",
      connectString: "10.41.190.196:1521/SISOQAS",
    });
    hayConexionGPC = "SI";

    arrayLineas = texto.split("\n");
    numLineas = arrayLineas.length;
    console.log("INICIO");
    for (i = 0; i < numLineas; i++) {
      linea = arrayLineas[i]; //se obtiene la información de la linea
      linea = linea.replace(/\|/g, "¬");
      arrayCampos = linea.split("¬"); //se carga el arreglo con todos los campos de la linea (separador de campo "|")
      etiqueta = arrayCampos[0]; // se obtiene la etiqueta que indica el tipo de linea (H,P,O,R,C) y el tipo de información de la linea
      //dependiendo del tipo de linea obtenemos la información necesaria

      console.log(
        "           ETIQUETA : " + etiqueta + "           LINEA :" + i
      );
      if (etiqueta == "H") {
        codTipo = arrayCampos[12];
        if (codTipo != "" && codTipo != null && codTipo != undefined) {
          codTipo = codTipo.trim();
        } else {
          textError = "El campo tipo de resultado no existe o es undefined";
          throw "ERROR";
        }

        if (codTipo == "2" || codTipo == "4") {
          esResuladoFinal = "SI";
        }
      }

      if (etiqueta == "O") {
        //Número de Petición
        numPeticion = arrayCampos[1];
        if (
          numPeticion != "" &&
          numPeticion != null &&
          numPeticion != undefined
        ) {
          numPeticion = numPeticion.trim();
        } else {
          textError =
            "[Etiqueta 0] El campo número de petición no existe o es undefined";
          throw "ERROR";
        }
        console.log("numPeticion");
        console.log(numPeticion);

        codPrueba = arrayCampos[2];
        console.log("codPrueba");
        console.log(codPrueba);

        if (codPrueba != "" && codPrueba != null && codPrueba != undefined) {
          codPrueba = codPrueba.trim();
        } else {
          textError =
            "[Etiqueta 0] El campo código de prueba no existe o es undefined";
          throw "ERROR";
        }

        //Nombre de la prueba
        nomPrueba = arrayCampos[3];
        if (nomPrueba != "" && nomPrueba != null && nomPrueba != undefined) {
          nomPrueba = nomPrueba.trim();
          nomPrueba = nomPrueba.toUpperCase();
        } else {
          textError =
            "[Etiqueta 0] El campo nombre de prueba no existe o es undefined";
          throw "ERROR";
        }

        //Fuente u origen de la prueba
        fuente = arrayCampos[9];
        if (fuente != "" && fuente != null && fuente != undefined) {
          fuente = fuente.trim();
        } else {
          textError =
            "[Etiqueta 0] El campo nombre de prueba no existe o es undefined";
          throw "ERROR";
        }

        codPerfil = codPrueba;

        // existePrueba
        if ((await existePrueba(numPeticion, codPerfil, connection)) == "NO") {
          // obtieneOrden
          console.log("No existe prueba -- > obtieneOrden");
          numOrden = await obtieneOrden(
            numPeticion,
            codPerfil,
            connectionMirth,
            connection
          );
          console.log(numOrden);
          resPrueba = "";
          tipoFila = 1;

          //registra_resultados_microbiologia
          console.log("registra_resultados_microbiologia tipoFila 1 -- 161");
          await registra_resultados_microbiologia(
            numPeticion,
            codPerfil,
            codPrueba,
            numOrden,
            nomPrueba,
            resPrueba,
            valor_cmi,
            tipoFila,
            connectionMirth,
            connection
          );

          hayTran = "SI";

          console.log(" obtieneOrden 2 -- linea 177");
          numOrden = await obtieneOrden(
            numPeticion,
            codPerfil,
            connectionMirth,
            connection
          );
          nomPrueba = "FUENTE:";
          resPrueba = fuente;
          tipoFila = 2;

          console.log("registra_resultados_microbiologia tipoFila 2 -- 188");
          await registra_resultados_microbiologia(
            numPeticion,
            codPerfil,
            codPrueba,
            numOrden,
            nomPrueba,
            resPrueba,
            valor_cmi,
            tipoFila,
            connectionMirth,
            connection
          );
          hayTran = "SI";
        }
      }
      if (etiqueta == "R") {
        subEtiqueta = arrayCampos[1];
        orden = arrayCampos[2];

        if (subEtiqueta == "E" || subEtiqueta == "G") {
          nomPrueba = arrayCampos[3];
          if (nomPrueba != "" && nomPrueba != null && nomPrueba != undefined) {
            nomPrueba = nomPrueba.trim();
            nomPrueba = nomPrueba.toUpperCase();
          } else {
            nomPrueba = "";
          }

          resPrueba = arrayCampos[4];
          if (resPrueba != "" && resPrueba != null && resPrueba != undefined) {
            resPrueba = resPrueba.trim();
            resPrueba = resPrueba.toLowerCase();
          } else {
            resPrueba = "";
          }
        } else if (subEtiqueta == "C") {
          conteoColonias = arrayCampos[2];
          if (
            conteoColonias != "" &&
            conteoColonias != null &&
            conteoColonias != undefined
          ) {
            conteoColonias = conteoColonias.trim();
          }
        } else if (subEtiqueta == "O") {
          cultivo = arrayCampos[2];
          if (cultivo != "" && cultivo != null && cultivo != undefined) {
            cultivo = cultivo.trim();
            cultivo = cultivo.toUpperCase();
          }
        } else if (subEtiqueta == "A") {
          nomPrueba = arrayCampos[3];
          if (nomPrueba != "" && nomPrueba != null && nomPrueba != undefined) {
            nomPrueba = nomPrueba.trim();
            nomPrueba = nomPrueba.toUpperCase();
          } else {
            nomPrueba = "";
          }

          resPrueba = arrayCampos[4];
          if (resPrueba != "" && resPrueba != null && resPrueba != undefined) {
            resPrueba = resPrueba.trim();
          } else {
            resPrueba = "";
          }

          resitencia = arrayCampos[5];
          if (
            resitencia != "" &&
            resitencia != null &&
            resitencia != undefined
          ) {
            resitencia = resitencia.trim();
          }
          if (resitencia == "I") {
            resitencia = "Intermedio";
          } else if (resitencia == "R") {
            resitencia = "Resistente";
          } else if (resitencia == "S") {
            resitencia = "Sensible";
          }

          cmi = resPrueba + " " + String.fromCharCode(181) + "g/mL";
          //resPrueba = "CMI: " + resPrueba + " " + String.fromCharCode(181) + "g/mL";
        } else if (subEtiqueta == "K" || subEtiqueta == "Y") {
          resPrueba = arrayCampos[2];
          if (resPrueba != "" && resPrueba != null && resPrueba != undefined) {
            resPrueba = resPrueba.trim();
            resPrueba = resPrueba.toLowerCase();
          } else {
            resPrueba = "";
          }
        }

        if (subEtiqueta == "E") {
          if (
            (await existePreliminar(
              numPeticion,
              codPerfil,
              nomPrueba,
              connectionMirth,
              connection
            )) == "NO"
          ) {
            if (Number(orden) == 1) {
              //En caso sea el primer registro se registra el título
              if (
                (await existePreliminar(
                  numPeticion,
                  codPerfil,
                  valorSubEtiquetaE,
                  connectionMirth,
                  connection
                )) == "NO"
              ) {
                numOrden = await obtieneOrden(
                  numPeticion,
                  codPerfil,
                  connectionMirth,
                  connection
                );
                //codPrueba = numOrden;
                nomPruebaTitulo = valorSubEtiquetaE;
                resPruebaTitulo = "";
                valor_cmi = "";
                tipoFila = 2;
                console.log(
                  "registra_resultados_microbiologia tipoFila 2 -- 315"
                );
                await registra_resultados_microbiologia(
                  numPeticion,
                  codPerfil,
                  codPrueba,
                  numOrden,
                  nomPruebaTitulo,
                  resPruebaTitulo,
                  valor_cmi,
                  tipoFila,
                  connectionMirth,
                  connection
                );
                hayTran = "SI";
              }
            }
            numOrden = await obtieneOrden(
              numPeticion,
              codPerfil,
              connectionMirth,
              connection
            );
            //codPrueba = numOrden;
            nomPrueba = nomPrueba; //initcap
            valor_cmi = "";
            tipoFila = 3;
            console.log("registra_resultados_microbiologia tipoFila 3 -- 341");
            await registra_resultados_microbiologia(
              numPeticion,
              codPerfil,
              codPrueba,
              numOrden,
              nomPrueba,
              resPrueba,
              valor_cmi,
              tipoFila,
              connectionMirth,
              connection
            );
            hayTran = "SI";
          }
        } else if (subEtiqueta == "G") {
          if (
            (await existePreliminar(
              numPeticion,
              codPerfil,
              nomPrueba,
              connectionMirth,
              connection
            )) == "NO"
          ) {
            if (Number(orden) == 1) {
              if (
                (await existePreliminar(
                  numPeticion,
                  codPerfil,
                  valorSubEtiquetaG,
                  connectionMirth,
                  connection
                )) == "NO"
              ) {
                numOrden = await obtieneOrden(
                  numPeticion,
                  codPerfil,
                  connectionMirth,
                  connection
                );
                //codPrueba = numOrden;
                nomPruebaTitulo = valorSubEtiquetaG;
                resPruebaTitulo = "";
                valor_cmi = "";
                tipoFila = 2;
                console.log(
                  "registra_resultados_microbiologia tipoFila 2 -- 387"
                );
                await registra_resultados_microbiologia(
                  numPeticion,
                  codPerfil,
                  codPrueba,
                  numOrden,
                  nomPruebaTitulo,
                  resPruebaTitulo,
                  valor_cmi,
                  tipoFila,
                  connectionMirth,
                  connection
                );
                hayTran = "SI";
              }
            }
            numOrden = await obtieneOrden(
              numPeticion,
              codPerfil,
              connectionMirth,
              connection
            );
            //codPrueba = numOrden;
            nomPrueba = nomPrueba; //initcap
            valor_cmi = "";
            tipoFila = 3;
            console.log("registra_resultados_microbiologia tipoFila 3 --413");
            await registra_resultados_microbiologia(
              numPeticion,
              codPerfil,
              codPrueba,
              numOrden,
              nomPrueba,
              resPrueba,
              valor_cmi,
              tipoFila,
              connectionMirth,
              connection
            );
            hayTran = "SI";
          }
        } else if (subEtiqueta == "C") {
          if (
            (await existePreliminar(
              numPeticion,
              codPerfil,
              valorSubEtiquetaC,
              connectionMirth,
              connection
            )) == "NO"
          ) {
            numOrden = await obtieneOrden(
              numPeticion,
              codPerfil,
              connectionMirth,
              connection
            );
            //codPrueba = numOrden;
            nomPrueba = valorSubEtiquetaC;
            resPrueba = conteoColonias;
            valor_cmi = "";
            tipoFila = 2;
            console.log("registra_resultados_microbiologia tipoFila 2 --449");
            await registra_resultados_microbiologia(
              numPeticion,
              codPerfil,
              codPrueba,
              numOrden,
              nomPrueba,
              resPrueba,
              valor_cmi,
              tipoFila,
              connectionMirth,
              connection
            );
            hayTran = "SI";
          }
        } else if (subEtiqueta == "O") {
          if (
            (await existeGermen(
              numPeticion,
              codPerfil,
              valorSubEtiquetaO,
              cultivo,
              connectionMirth,
              connection
            )) == "NO"
          ) {
            resPrueba = cultivo;
            numOrden = await obtieneOrden(
              numPeticion,
              codPerfil,
              connectionMirth,
              connection
            );
            //codPrueba = numOrden;
            nomPrueba = valorSubEtiquetaO;
            resPrueba = cultivo;
            resFinal = cultivo;
            valor_cmi = "";
            tipoFila = 2;
            console.log("registra_resultados_microbiologia tipoFila 2 --488");
            await registra_resultados_microbiologia(
              numPeticion,
              codPerfil,
              codPrueba,
              numOrden,
              nomPrueba,
              resPrueba,
              valor_cmi,
              tipoFila,
              connectionMirth,
              connection
            );
            hayTran = "SI";
          }
        } else if (subEtiqueta == "A" && codTipo == "4") {
          if (
            (await existePreliminar(
              numPeticion,
              codPerfil,
              nomPrueba,
              connectionMirth,
              connection
            )) == "NO"
          ) {
            if (Number(orden) == 1) {
              if (
                (await existePreliminar(
                  numPeticion,
                  codPerfil,
                  valorSubEtiquetaA,
                  connectionMirth,
                  connection
                )) == "NO"
              ) {
                numOrden = await obtieneOrden(
                  numPeticion,
                  codPerfil,
                  connectionMirth,
                  connection
                );
                //codPrueba = numOrden;
                nomPruebaTitulo = valorSubEtiquetaA;
                resPruebaTitulo = "";
                valor_cmi = "";
                tipoFila = 2;
                console.log(
                  "registra_resultados_microbiologia tipoFila 2 --534"
                );
                await registra_resultados_microbiologia(
                  numPeticion,
                  codPerfil,
                  codPrueba,
                  numOrden,
                  nomPruebaTitulo,
                  resPruebaTitulo,
                  valor_cmi,
                  tipoFila,
                  connectionMirth,
                  connection
                );
                hayTran = "SI";

                numOrden = await obtieneOrden(
                  numPeticion,
                  codPerfil,
                  connectionMirth,
                  connection
                );
                //codPrueba = numOrden;
                nomPruebaTitulo = "Antimicrobiano";
                resPruebaTitulo = "Interpretación ";
                valor_cmi = "CIM";
                tipoFila = 2;
                console.log(
                  "registra_resultados_microbiologia tipoFila 2 -- 560"
                );
                await registra_resultados_microbiologia(
                  numPeticion,
                  codPerfil,
                  codPrueba,
                  numOrden,
                  nomPruebaTitulo,
                  resPruebaTitulo,
                  valor_cmi,
                  tipoFila,
                  connectionMirth,
                  connection
                );
                hayTran = "SI";
              }
            }
            numOrden = await obtieneOrden(
              numPeticion,
              codPerfil,
              connectionMirth,
              connection
            );
            //codPrueba = numOrden;
            resPrueba = resitencia;
            valor_cmi = cmi;
            tipoFila = 3;
            console.log("registra_resultados_microbiologia tipoFila 3 --586");
            await registra_resultados_microbiologia(
              numPeticion,
              codPerfil,
              codPrueba,
              numOrden,
              nomPrueba,
              resPrueba,
              valor_cmi,
              tipoFila,
              connectionMirth,
              connection
            );
            hayTran = "SI";
          }
        } else if (subEtiqueta == "K") {
          if (
            (await existePreliminar(
              numPeticion,
              codPerfil,
              valorSubEtiquetaK,
              connectionMirth,
              connection
            )) == "NO"
          ) {
            numOrden = await obtieneOrden(
              numPeticion,
              codPerfil,
              connectionMirth,
              connection
            );
            //codPrueba = numOrden;
            nomPrueba = valorSubEtiquetaK;
            valor_cmi = "";
            tipoFila = 2;
            console.log("registra_resultados_microbiologia tipoFila 2 --621");
            await registra_resultados_microbiologia(
              numPeticion,
              codPerfil,
              codPrueba,
              numOrden,
              nomPrueba,
              resPrueba,
              valor_cmi,
              tipoFila,
              connectionMirth,
              connection
            );
            hayTran = "SI";
          }
        } else if (subEtiqueta == "Y") {
          if (
            (await existePreliminar(
              numPeticion,
              codPerfil,
              valorSubEtiquetaY,
              connectionMirth,
              connection
            )) == "NO"
          ) {
            numOrden = await obtieneOrden(
              numPeticion,
              codPerfil,
              connectionMirth,
              connection
            );
            //codPrueba = numOrden;
            nomPrueba = valorSubEtiquetaY;
            valor_cmi = "";
            tipoFila = 2;
            console.log("registra_resultados_microbiologia tipoFila 2 --656");
            await registra_resultados_microbiologia(
              numPeticion,
              codPerfil,
              codPrueba,
              numOrden,
              nomPrueba,
              resPrueba,
              valor_cmi,
              tipoFila,
              connectionMirth,
              connection
            );
            hayTran = "SI";
          }
        } else if (subEtiqueta == "R") {
          logger.info("sub - etiqueta R");
          resultado = arrayCampos[2];
          if (resultado != "" && resultado != null && resultado != undefined) {
            resultado = resultado.trim();
            resultado = resultado.toUpperCase();
          }
          console.log("existePreliminar -- 678");
          if (codTipo == "4") {
            if (
              (await existePreliminar(
                numPeticion,
                codPerfil,
                valorSubEtiquetaR,
                connectionMirth,
                connection
              )) == "NO"
            ) {
              numOrden = await obtieneOrden(
                numPeticion,
                codPerfil,
                connectionMirth,
                connection
              );
              //codPrueba = numOrden;
              nomPrueba = valorSubEtiquetaR;
              resPrueba = resultado;
              resFinal = resultado;
              valor_cmi = "";
              tipoFila = 2;
              console.log("registra_resultados_microbiologia tipoFila 2 --701");
              await registra_resultados_microbiologia(
                numPeticion,
                codPerfil,
                codPrueba,
                numOrden,
                nomPrueba,
                resPrueba,
                valor_cmi,
                tipoFila,
                connectionMirth,
                connection
              );
              hayTran = "SI";
            }
          } else if (codTipo == "1" || codTipo == "2") {
            console.log("resultado tipo 1");
            if (resultado.indexOf("NEG") != "-1") {
              console.log("resultado NEG ");
              esResuladoFinal = "SI";
              //obtieneOrden
              numOrden = await obtieneOrden(
                numPeticion,
                codPerfil,
                connectionMirth,
                connection
              );
              nomPrueba = valorSubEtiquetaR;
              resPrueba = resultado;
              resFinal = resultado;
              valor_cmi = "";
              tipoFila = 2;
              // registra_resultados_microbiologia
              console.log("registra_resultados_microbiologia tipoFila 2 --734");
              await registra_resultados_microbiologia(
                numPeticion,
                codPerfil,
                codPrueba,
                numOrden,
                nomPrueba,
                resPrueba,
                valor_cmi,
                tipoFila,
                connectionMirth,
                connection
              );
              hayTran = "SI";
            }
          }
        }
      }
    }
    console.log("registra_resultado -- 752" + resFinal);
    if (esResuladoFinal == "SI") {
      await registra_resultado(
        numPeticion,
        codPrueba,
        resFinal,
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        connectionMirth,
        connection
      );
      console.log("actualiza_estado_peticion -- 752");
      await actualiza_estado_peticion(numPeticion, connection);
    }
    connection.close();
  } catch (error) {
    existeError = "SI";

    if (hayConexionGPC == "NO") {
      console.log(
        " ERROR al conectar con la BD de la web de resultados ==> " +
          error.message
      );
    } else {
      if (error == "ERROR") {
        msgError = textError;
      } else {
        msgError = error.message;
      }
      connection.close();
    }
    var msgLog = "NumPet: " + numPeticion + " Error: " + msgError;
    console.log(msgLog);
  }
}

//Función que valida si la prueba existe o no en la peticion origen

async function existePrueba(pn_id_peticion, pn_id_prestacion, connection) {
  const resultado = await connection.execute(
    "select pq_webresultados_datos.fn_exist_prest_micro(" +
      pn_id_peticion +
      "," +
      pn_id_prestacion +
      ")from dual"
  );
  var resultado_final = resultado.rows[0][0];
  if (resultado_final > 0) {
    return "SI";
  } else {
    return "NO";
  }
}

async function obtieneOrden(
  pn_id_peticion,
  pn_id_prestacion,
  connectionMirth,
  connection
) {
  const sql = await connectionMirth.execute(
    "select v_c_his as codLab, v_f_generafile as generaFile, v_c_vitek as procesaVitek from mirth_worka_prd1.ome_equivalencias_micro where v_c_laboratorio = '" +
      pn_id_prestacion +
      "' "
  );
  var CODLABN = sql.metaData.findIndex((x) => x.name === "CODLAB");

  var codLab = sql.rows[0][CODLABN];

  const resultado = await connection.execute(
    "select pq_webresultados_datos.fn_obtener_orden_micro(" +
      pn_id_peticion +
      "," +
      codLab +
      ")from dual"
  );

  return resultado.rows[0][0];
}

async function registra_resultados_microbiologia(
  pn_id_peticion,
  pn_id_prestacion,
  pn_id_prueba,
  pn_id_orden,
  pv_nom_prueba,
  pv_resultado,
  pv_valor,
  pn_tipo_fila,
  connectionMirth,
  connection
) {
  //Obtenemos el código de la tabla intermedia

  const sql = await connectionMirth.execute(
    "select v_c_his as codLab, v_f_generafile as generaFile, v_c_vitek as procesaVitek from mirth_worka_prd1.ome_equivalencias_micro where v_c_laboratorio = '" +
      pn_id_prueba +
      "' "
  );
  console.log(sql);
  var CODLABN = sql.metaData.findIndex((x) => x.name === "CODLAB");

  var codLab = sql.rows[0][CODLABN];
  var consulta = "";
  consulta =
    "BEGIN " +
    "WEBRESINT.PQ_WEBRESULTADOS_DATOS.PR_INSERT_RESULT_MICRO(" +
    pn_id_peticion +
    "," +
    codLab +
    "," +
    codLab +
    "," +
    pn_id_orden +
    ",'" +
    pv_nom_prueba +
    "','" +
    pv_resultado +
    "','" +
    pv_valor +
    "'," +
    pn_tipo_fila +
    ");" +
    " END;";
  console.log(consulta);
  const resultado = await connection.execute(consulta);
  /*const resultado = await connection.execute(
    "BEGIN webresint.pq_webresultados_datos.pr_insert_result_micro(30055844,20,20,10030,'ISIQUE','','',1); END;"
  );*/

  console.log(resultado);
}

async function existePreliminar(
  pn_id_peticion,
  pn_id_prestacion,
  pv_nom_prueba,
  connectionMirth,
  connection
) {
  var resultado = "";
  var sql = "";

  //Obtenemos el código de la tabla intermedia

  var sql = await connectionMirth.execute(
    "select v_c_his as codLab, v_f_generafile as generaFile, v_c_vitek as procesaVitek from mirth_worka_prd1.ome_equivalencias_micro where v_c_laboratorio = '" +
      pn_id_prestacion +
      "' "
  );
  console.log(sql);
  var CODLABN = sql.metaData.findIndex((x) => x.name === "CODLAB");

  var codLab = sql.rows[0][CODLABN];

  resultSQL = dbConnMIRTH.executeCachedQuery(sql);
  resultSQL.next();
  codLab = resultSQL.getString("codLab");

  var resultado = await connection.execute(
    "select webresint.pq_webresultados_datos.fn_exist_preliminar_micro(" +
      pn_id_peticion +
      "," +
      codLab +
      ",'" +
      pv_nom_prueba +
      "')from dual"
  );

  console.log(resultado);
  if (resultado > 0) {
    return "SI";
  } else {
    return "NO";
  }
}

async function existeGermen(
  pn_id_peticion,
  pn_id_prestacion,
  pv_nom_prueba,
  pv_resultado,
  connectionMirth,
  connection
) {

  //Obtenemos el código de la tabla intermedia
  const sql = await connectionMirth.execute(
    "select v_c_his as codLab, v_f_generafile as generaFile, v_c_vitek as procesaVitek from mirth_worka_prd1.ome_equivalencias_micro where v_c_laboratorio = '" +
      pn_id_prestacion +
      "' "
  );
  console.log(sql);
  var CODLABN = sql.metaData.findIndex((x) => x.name === "CODLAB");
  var codLab = sql.rows[0][CODLABN];

  const resultado = await connection.execute(
    "select webresint.pq_webresultados_datos.fn_exist_germen_micro(" +
      pn_id_peticion +
      "," +
      codLab +
      "," +
      pv_nom_prueba +
      "," +
      pv_resultado +
      ")from dual"
  );
  console.log(resultado);
  console.log(resultado.rows[0][0]);
  var result = resultado.rows[0][0];
  if (result > 0) {
    return "SI";
  } else {
    return "NO";
  }
}

async function actualiza_estado_peticion(pn_id_peticion, connection) {
  const resultado = await connection.execute(
    "BEGIN webresint.pq_webresultados_datos.pr_update_estado(" +
      pn_id_peticion +
      "); END;"
  );

  console.log(resultado);
}

async function registra_resultado(
  pn_id_peticion,
  pn_id_prueba,
  pv_resultado,
  pv_uni_med,
  pn_normal_min,
  pn_normal_max,
  pv_fecha_toma,
  pv_fecha_res,
  pv_id_nivel_prueba,
  pn_critico_min,
  pn_critico_max,
  pv_literal,
  connectionMirth,
  connection
) {
  //Obtenemos el código de la tabla intermedia

  const sql = await connectionMirth.execute(
    "select v_c_his as codLab, v_f_generafile as generaFile, v_c_vitek as procesaVitek from mirth_worka_prd1.ome_equivalencias_micro where v_c_laboratorio = '" +
      pn_id_prueba +
      "' "
  );
  console.log(sql);
  var CODLABN = sql.metaData.findIndex((x) => x.name === "CODLAB");

  var codLab = sql.rows[0][CODLABN];

  const resultado = await connection.execute(
    "BEGIN webresint.pq_webresultados_datos.pr_update_resultados(" +
      pn_id_peticion +
      ",'" +
      codLab +
      "','" +
      pv_resultado +
      "','" +
      pv_uni_med +
      "','" +
      pn_normal_min +
      "','" +
      pn_normal_max +
      "','" +
      pv_fecha_toma +
      "','" +
      pv_fecha_res +
      "','" +
      pv_id_nivel_prueba +
      "','" +
      pn_critico_min +
      "','" +
      pn_critico_max +
      "','" +
      pv_literal +
      "'); END;"
  );

  console.log(resultado);

  const result = await connection.execute(
    `BEGIN
       webresint.pq_webresultados_datos.pr_update_resultados(:pn_id_peticion, :codLab , :pv_resultado, :pv_uni_med, :pn_normal_min, :pn_normal_max, :pv_fecha_toma, :pv_fecha_res, :pv_id_nivel_prueba, :pn_critico_min, :pn_critico_max, :pv_literal);
     END;`,
    {
      pn_id_peticion: pn_id_peticion,
      codLab: codLab,
      pv_resultado: pv_resultado,
      pv_uni_med: pv_uni_med,
      pn_normal_min: pn_normal_min,
      pn_normal_max: pn_normal_max,
      pv_fecha_toma: pv_fecha_toma,
      pv_fecha_res: pv_fecha_res,
      pv_id_nivel_prueba: pv_id_nivel_prueba,
      pn_critico_min: pn_critico_min,
      pn_critico_max: pn_critico_max,
      pv_literal: pv_literal,
    }
  );

  console.log(result);
}

async function pruebas() {
  let connection;
  let connectionMirth;
  var hayConexionGPC = "NO";

  try {
    connectionMirth = await oracledb.getConnection({
      user: "MIRTHPRD23",
      password: "M1r7H4unA23",
      connectString: "10.41.1.169:1521/PRD23",
    });

    // CONEXION WEBRESINT
    connection = await oracledb.getConnection({
      user: "WEBRESINT",
      password: "WEBRESINT2017",
      connectString: "10.41.190.196:1521/SISOQAS",
    });

    /* await registra_resultado(
      30055844,
      9002,
      "ESCHERICHIA COLI x2",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      connectionMirth,
      connection
    ); */
    await actualiza_estado_peticion(30055844,connection);
  } catch (error) {
    console.log("error");
    console.log(error);
  }
}

//data();
pruebas();
