const oracledb = require("oracledb");

async function controller(){
    try{
        let connection;
        connection = await oracledb.getConnection({
            user: "WEBRESINT",
            password: "WEBRESINT2017",
            connectString: "10.41.190.196:1521/SISOQAS"
        });

        var result = await connection.execute("SELECT P.ID_PETICION AS id_peticion FROM PETICION P WHERE P.AMBITO IN ('H','U') AND P.VISIBLE = 0 AND P.TIPO_PETICION = 'L' AND P.FECHA_TOMA >= TO_DATE ('29/01/2019','DD/MM/YYYY')");
        //console.log(result)
        connection.close();
        return result;
    
    }catch(e){
    
        console.log("WebResultados-ConsultaPeticionesWR: ERROR al conectar con la BD del WR ("+e.message+")");
    
        var ListaVacia = "" //new Packages.java.util.ArrayList();
        return ListaVacia;
    
    }
}

async function manejo_errores(){

    var array_id_peticion = await controller();
    
    //Variables de Conexión
    var hayConexionHIS = "NO"; var hayConexionWR = "NO";
    var dbConnWR; var dbConnHIS;

    //Variables de error
    var existeError = "NO"; codError = 0;

    dbConnHIS = await oracledb.getConnection({
        user: "xhisprd",
        password: "HIS510",
        connectString: "10.41.1.62:1521/HIS510"
    }); 
    hayConexionHIS = "SI";

    dbConnWR = await oracledb.getConnection({
        user: "WEBRESINT",
        password: "WEBRESINT2017",
        connectString: "10.41.190.196:1521/SISOQAS"
    }); 
    hayConexionWR = "SI";

    try{

        for (let i=0; i<1; i++){
            //console.log(array_id_peticion.rows[i][0])
            var tieneAlta = await actualizar_peticion(dbConnHIS,dbConnWR,array_id_peticion.rows[i][0]);
            console.log(tieneAlta)
        }
        
        //var tieneAlta = actualizar_peticion(dbConnHIS, dbConnWR, $('id_peticion'));
        dbConnHIS.close();
        dbConnWR.close();
    }
    catch(e){
        existeError = "SI";
        if (hayConexionHIS == "NO"){
            existeError = "NOhayConexionHIS";
            console.log("WebResultados-ActualizarPeticionesHIS: ERROR al conectar con la BD del HIS ("+e.message+")");
        } else if (hayConexionWR == "NO"){
            existeError = "NOhayConexionWR";
            console.log("WebResultados-ActualizarPeticionesWS: ERROR al conectar con la BD del WR ("+e.message+")");
        } else{
            existeError = e.message;
            console.log("WebResultados-ActualizarPeticiones: ERROR en las funciones: ("+e.message+")");
        }
    }       

}

async function actualizar_peticion(dbConnHIS,dbConnWR,idPeticion){
    var sql1 = "select peticion_alta("+idPeticion+") from dual";
    var visible = 1;
	var resultado = 0;

	//llamas a la funcion
    var callable_stmt1 = await dbConnHIS.execute(sql1);

	// Se recoge el resultado
	var tieneAlta  =  callable_stmt1.rows[0][0];
    //console.log(tieneAlta)
		
	if( tieneAlta == 1){  //validacion 1: alta,  0: no alta
		//aca llama el procedure de actualizar 
		var sql2 = "DECLARE PN_RESPONSE NUMBER; BEGIN pq_webresultados_datos.pr_update_peticion("+idPeticion+","+visible+",PN_RESPONSE); END;" //dbConnWR.prepareCall(sql2);

        console.log(sql2)

        var callable_stmt2 = await dbConnWR.execute(sql2);

        console.log(callable_stmt2);

        dbConnWR.close();
	
		// callable_stmt2.setInt(1, idPeticion); //pn_id_peticion
		// callable_stmt2.setInt(2, visible); //pn_visible
		// callable_stmt2.registerOutParameter(3, java.sql.Types.INTEGER);
		
		// callable_stmt2.execute();
		// resultado = callable_stmt2.getInt(3); //Cantidad de update actualizados 
        // callable_stmt2.close(); 
	}	
	
	//callable_stmt1.close();

	return tieneAlta;//Devuelve si el paciente esta de alta(1) o no(0)
}

// async function hola(){
//     const sol = await controller();
//     console.log(sol)
// }
// hola()
manejo_errores()