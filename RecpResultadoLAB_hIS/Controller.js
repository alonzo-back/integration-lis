const oracledb = require("oracledb");

async function controller(msgOrigen){

    //variables de error
    var existeError = "NO"; var msgError = "";

    var linea=""; var arrayCampos; var campo; var arrayDatos; var etiqueta; 
    var hayTran = "NO"; var hayConn = "NO";

    //variable de la tira P(Paciente)
    var numPeticion = "";

    //variables de la tira R(Resultados)
    var codPrueba = ""; var resultado = ""; var unidades = ""; var valInf = ""; 
    var valSup = ""; var fec_validacion = ""; var fec_toma = ""; var valInf_panico = "";
    var valSup_panico = ""; var alarmar = ""; var resLiteral =  "";

    var msgOrigen ; var numLineas = "";
    var arrayLineas; // arreglo que contiene por cada posición una linea del mensage origina

    try {

        let connection;
        connection = await oracledb.getConnection({
            user: "WEBRESINT",
            password: "WEBRESINT2017",
            connectString: "10.41.190.196:1521/SISOQAS"
        });

        hayConn = "SI";

        arrayLineas = msgOrigen.split('\n');
        numLineas = arrayLineas.length;	

        for (j=0; j< numLineas; j++) { //se recorre el mensaje linea por linea
            linea = arrayLineas[j]; //se obtiene la información de la linea
            linea = linea.replace(/\|/g,"¬")
            arrayCampos = linea.split("¬"); //se carga el arreglo con todos los campos de la linea (separador de campo "|")
            etiqueta = arrayCampos[0]; // se obtiene la etiqueta que indica el tipo de linea (H,P,O,R,C) y el tipo de información de la linea
            if (etiqueta == "P"){
                campo = arrayCampos[2];
                campo = campo.replace(/\^/g,"¬");
                arrayDatos = campo.split("¬");
                
                // [Número de petición]
                numPeticion = arrayDatos[0];
                if (numPeticion != undefined){numPeticion = numPeticion.trim();}
            }
            
            if (etiqueta == "R"){
                campo = arrayCampos[2];
                if (campo != "" && campo != undefined) {
                    campo = campo.replace(/\^/g,"¬");
                    arrayDatos = campo.split("¬");
    
                    // [Código de prueba]
                       codPrueba = arrayDatos[3];
                    if (codPrueba != undefined){codPrueba = codPrueba.trim();}
                       else{codPrueba = "";}
                }
                else{codPrueba = "";}
                // [Resultado de prueba]
                resultado = arrayCampos[3]
                if (resultado != "" && resultado != undefined){
                    resultado = resultado.trim();
                    resultado = resultado.replace(/\''/g,"seg");
                    resultado = resultado.replace(/\'/g,"min");
                }
                else{resultado = "";}
    
                // [Unidades]
                unidades = arrayCampos[4];
                if (unidades != "" && unidades != undefined){ unidades = unidades.trim(); }
                else{unidades = "";}
    
                campo = arrayCampos[5];
                if (campo != "" && campo != undefined){
                    campo = campo.replace(/\^/g,"¬");
                    arrayDatos = campo.split("¬");
                // [Valor inferior]
                    valInf = arrayDatos[0]; 
                    if (valInf != undefined){ valInf = valInf.trim(); }
                    else {valInf = "";}

                // [Valor superior]
                    valSup = arrayDatos[1]; 
                    if (valSup != undefined){valSup = valSup.trim();}
                    else{valSup = "";}
                }
                else{ valInf = ""; valSup = "";}
    
                if (valInf != ""){valInf = valInf.replace(/\,/g,"."); valInf = parseFloat(valInf);}
                   if (valSup != ""){valSup = valSup.replace(/\,/g,"."); valSup = parseFloat(valSup);}
                
                // [Alarma de resultados]
                alarma = arrayCampos[6];
                if (alarma != "" && alarma != undefined){ alarma = alarma.trim(); }
                else{alarma = "";}
            
                // [Fecha y hora de validacion de la muestra]
                campo = arrayCampos[13];
                if (campo != "" && campo != undefined){
                    fec_validacion = campo;
                    fec_validacion = fec_validacion.trim();
                    fec_validacion = fec_validacion.substring(6,8) + "/" + fec_validacion.substring(4,6) + "/" + fec_validacion.substring(0,4);
                       //fec_validacion.substring(8,10) + ":" + fec_validacion.substring(10,12) + ":" + fec_validacion.substring(12,14);
                }
                else{fec_validacion = "";}
    
                // [Fecha y hora de toma de la muestra]
                campo = arrayCampos[14];
                if (campo != "" && campo != undefined){
                    fec_toma = campo;
                    fec_toma = fec_toma.trim();
                    fec_toma = fec_toma.substring(6,8) + "/" + fec_toma.substring(4,6) + "/" + fec_toma.substring(0,4);
                    //fec_toma.substring(8,10) + ":" + fec_toma.substring(10,12) + ":" + fec_toma.substring(12,14);
                }
                else{fec_toma = "";}
    
                // [Valores de panico]
                campo = arrayCampos[18];
                if (campo != "" && campo != undefined){
                    campo = campo.replace(/\^/g,"¬");
                    arrayDatos = campo.split("¬");
                // [Valor inferior]
                    valInf_panico = arrayDatos[0]; 
                    if (valInf != undefined){ valInf_panico = valInf_panico.trim(); }
                    else {valInf_panico = "";}

                // [Valor superior]
                    valSup_panico = arrayDatos[1]; 
                    if (valSup_panico != undefined){valSup_panico = valSup_panico.trim();}
                    else{valSup_panico = "";}
                }
                else{ valInf_panico = ""; valSup_panico = "";}

                // [Alarma de resultados]
                resLiteral = arrayCampos[19];
                if (resLiteral != "" && resLiteral != undefined){ resLiteral = resLiteral.trim(); }
                else{resLiteral = "";}
                registra_resultado(	numPeticion, codPrueba, resultado, unidades, valInf, valSup,  fec_toma, fec_validacion, alarma, valInf_panico, valSup_panico, resLiteral); 
               
                hayTran = "SI";
            }
        }

        actualiza_estado_peticion(numPeticion);

    } catch (error) {
        existeError = "SI";

        if (hayConn == "NO"){
            console.log("WebResultados-RecepcionResultadosHIS: ERROR al conectar con la BD de la Web de Resultados ("+e.message+")");		
        }
        else{
            msgError = e.message
            if (hayTran == "SI"){connection.rollback();}
            connection.close();
        }
        var msgLog = "NumPet: " +numPeticion + " Error: " + msgError;
        console.log(msgLog);
    }

}

async function registra_resultado(pn_id_peticion, pn_id_prueba, pv_resultado, pv_uni_med, 
    pn_normal_min, pn_normal_max, pv_fecha_toma, pv_fecha_res,
    pv_id_nivel_prueba, pn_critico_min, pn_critico_max, pv_literal
    ){
        var sql = "";
        sql += "BEGIN ";
        sql += "webresint.pq_webresultados_datos.pr_update_resultados("+pn_id_peticion+", "+pn_id_prueba+", '"+pv_resultado+"', '"+
        pv_uni_med+"', "+pn_normal_min+", "+pn_normal_max+", '"+pv_fecha_toma+"', '"+pv_fecha_res+"', '"+pv_id_nivel_prueba+"', "+pn_critico_min+", "+
        pn_critico_max+", '"+pv_literal+"'); ";
        sql += "END;"

        console.log(sql)

        try{
            let connection;
            connection = await oracledb.getConnection({
                user: "WEBRESINT",
                password: "WEBRESINT2017",
                connectString: "10.41.190.196:1521/SISOQAS"
            });

            //console.log(connection)
            const result = await connection.execute(sql)
            //const result = await connection.execute("begin webresint.pq_webresultados_datos.pr_update_resultados(4100000003,401234,'6.00','Cel/uLasasas',5,12,'12/04/2019','12/04/2019','',null,null,'');end;")
            console.log(result)

            //connection.close()
            
            console.log("final")
            //console.log(result)

            //call WEBRESINT.pq_webresultados_datos.pr_update_resultados(4100000007,401234,'6.00','Cel/uL',5,12,'12/04/2019','12/04/2019',null,null,null,null)

            
            // const result = await connection.execute("CALL WEBRESINT.pq_webresultados_datos.pr_update_resultados("+pn_id_peticion+conector+pn_id_prueba+conector+pv_resultado+conector+
            // pv_uni_med+conector+pn_normal_min+conector+pn_normal_max+conector+pv_fecha_toma+conector+pv_fecha_res+conector+pv_id_nivel_prueba+conector+pn_critico_min+conector+
            // pn_critico_max+conector+pv_literal+")");

        }catch(e){console.log(e)}

}


async function actualiza_estado_peticion(pn_id_peticion){
    var sql = "begin webresint.pq_webresultados_datos.pr_update_estado("+pn_id_peticion+"); end;";

    try{
        let connection;
        connection = await oracledb.getConnection({
            user: "WEBRESINT",
            password: "WEBRESINT2017",
            connectString: "10.41.190.196:1521/SISOQAS"
        });

        const result = await connection.execute(sql);
        console.log(result)
        console.log("QUERY EJECUTADO. COMANDO EXITOSO!")

    }catch(error){
        console.log("ERROR: VALIDAR EL SIGUIENTE MENSAJE")
        console.log(error)
    }
}

//actualiza_estado_peticion(4100000005)

registra_resultado(4100000003,401234,'6.00','Cel/uLAAAc',5,12,'12/04/2019','12/04/2019','',0,0,'')

// controller("H|\^&|2284481||Omega v4^Roche Diagnostics|||||Omega^Roche Diagnostics||P||20190812100605\n"+
// "P|1|10018634|33832||BERNAL^MAMANI^KATHERINE MABEL||19850312|F|||||029019^PORTUGAL PORTUGAL EDWARD MIGUEL||||||||10128603||20190603085836^000000|||||||||89 ^VSU GINECO-OBSTETRICIA CEX|C^HIS|||1\n"+
// "O|1|10018634^G||^^^128|R|20190603085836|||||A||||SANGRE||||||||||O\n"+
// "R|1|^^^128^305^^^^^TIEMPO DE COAGULACION Y SANGRIA|-||||128^TIEMPO DE COAGULACION Y SANGRI|F||^GPARIENTE||20190603085819|20190812100533|20190812092603|20190812092621|20190812092705|158^HEMATOLOGIA\n"+
// "O|2|10018634^G||^^^3560|R|20190603085836|||||A||||SANGRE||||||||||O\n"+
// "R|2|^^^3560^306^^^^^Tiempo de sangría|2.00||1.00^4.00||128^TIEMPO DE COAGULACION Y SANGRI|F||^GPARIENTE||20190812100533|20190812100533|20190812092603|20190812092621|20190812092705|158^HEMATOLOGIA\n"+
// "O|3|10018634^G||^^^3565|R|20190603085836|||||A||||SANGRE||||||||||O\n"+
// "R|3|^^^3565^307^^^^^Tiempo de coagulación|6.00||5.00^12.00||128^TIEMPO DE COAGULACION Y SANGRI|F||^GPARIENTE||20190812100533|20190812100533|20190812092603|20190812092621|20190812092705|158^HEMATOLOGIA\n"+
// "L|1|N");
